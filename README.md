# NSS_DU6


<!-- list autorů -->
## Authors

- [Anna Kachmasheva](https://gitlab.fel.cvut.cz/kachmann)

<!-- list technologii -->
## Used technologies

- JS
- HTML
- CSS

<!-- popis -->
## Description

This should be a description of the project...

### Project goals
| Goal | Description                      |
|:---------|:---------------------------------|
| Goal 1   | Description text. |
| Goal 2    | Description text. <sup>1</sup>   |
1. text.

<!-- nazev instalace -->
## Setup

1) **clone** this repository. 
2) _navigate_ to the top level of the repository.
3) *open* main.ja in your browser.

<!-- copyright -->
## Сopyright
